# Import der vorausgesetzten Bibliotheken
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

##################################################################################################
# Diese kleine Bibliothek soll folgendes leisten:
#   1.) Die in den oben vorausgesetzten Bibliotheken nicht enthaltenen aber im Modul ML 
#       benoetigten Funktionalitaeten bereitstellen.
#   2.) Funktionalitaeten, die in den vorausgesetzten
#       Bibliotheken enthalten sind mit einem "selbsterklaerenden Namen"
#       versehen.
#
#
# Diese Source-Datei kann direkt im GoogleDrive editiert werden. Hierzu bietet sich der 
# im GoogleDrive bei "Oeffnen mit" angebotene "Texteditor" an.
# Zu beachten ist jedoch Folgendes:
#   1.) Nachdem die geaenderte Datei mittels "Save to Drive" abgespeichert wurde
#       steht diese den Jupyter-Notebooks (wegen Cloud-Synchronisation) oftmals nicht sofort
#       zur Verfuegung, d.h. die Jupyter-Notebooks ARBEITEN dann NOCH mit der ALTEN VERSION !!!!
#       Daher ist es ratsam die Ausgabe der Funktion vers() anzupassen und anhand
#       deren Ausgabe im Jupyter-Notebook die Aktualitaet der verwendeten ML-Lib
#       zu ueberpruefen
#   2.) Damit ein Jupyter-Notebook eine Bilbliothek neu laedt muss 
#       die Laufzeitumgebung neu gestartet werden. Es bietet sich somit an folgendes einzugeben
#           Laufzeit -> Neu starten und alle ausfuehren
#       Selbst wenn man dies macht ist der Punkt 1) immer noch zu beruecksichtigen !!!!
################################################################################################## 



def vers():
    ver=0.92
    return (ver)


##################################################################################################
#                                                                                                #
#                                 Trainigsdatenverwaltung                                        #
#                                                                                                #
##################################################################################################

##################################################################################################
#                 Konvertierung eines Pandas-Dataframe zu einer Numpy-Matrix                     #
##################################################################################################
def pandas_to_numpy_matrix(pandas, select_cols):
  
  npmatrix = pandas.loc[  : , select_cols].values
  
  return npmatrix  

##################################################################################################
#     Trainingsdaten in Train-, Validate- und Test-Datenssaetze ZUFAELLIG  aufteilen              #
##################################################################################################
#    Zufälliges Splitten zweier Matrizen X und y (i.d.R: X=Eingabedaten, y=Ausgabedaten) entlang Ihrer Zeilen
#    Beide Matrizen müssen die gleiche Anzahl von Zeilen haben !!!
#    Hierbei werden beide Matrizen gleich geshuffelt, d.h. die geshuffelten Ausgabewerte von y passen immer noch
#    zu den geshuffelten Werten von X
#    Folgenden Ausgaben werden für X und y erzeugt:
#      training:     Trainingsdatensatz der aus den ersten percent_train (z.B. 60 von 100) Zeilen von shuffle besteht
#      test:         Testdatensatz der aus den ersten danach folgenden percent_test (z.B. 15 von 100) Zeilen von shuffle besteht
#      validation:   Validierungsdatensatz der aus den restlichen Zeilen (z.B. 25 von 100) von shuffle besteht
def split_matrix_horizontal_randomly(X, y, percent_train=70, percent_test=15):

  shuffeld = np.hstack((X,y))
  percent_validation = 100 - percent_train - percent_test

  cols_X = X.shape[1]
  rows = X.shape[0]
  
  np.random.shuffle(shuffeld)               # Mischen der Zeilen der Matrix shuffeld
  
  end_training = int(rows*percent_train/100)
  end_testing = end_training + int((rows * percent_test/100))
  
  X_training = shuffeld[: end_training  , :cols_X]
  X_testing = shuffeld[end_training:end_testing  , :cols_X]
  X_validation = shuffeld[end_testing:  , :cols_X]

  y_training = shuffeld[:end_training , cols_X:]
  y_testing = shuffeld[end_training:end_testing , cols_X:]
  y_validation = shuffeld[end_testing:  , cols_X:]
  
  return X_training, X_testing, X_validation, y_training, y_testing, y_validation


##################################################################################################
#     Trainingsdaten in Train-, Validate- und Test-Datenssetze GEMAESS ANORDNUNG  aufteilen     
##################################################################################################
#    Splitten zweier Matrizen X und y (i.d.R: X=Eingabedaten, y=Ausgabedaten) entlang Ihrer Zeilen
#    Beide Matrizen müssen die gleiche Anzahl von Zeilen haben !!!
#    Folgenden Ausgaben werden für X und y erzeugt:
#      training:     Trainingsdatensatz der aus den ersten percent_train (z.B. 60 von 100) Zeilen von shuffle besteht
#      test:         Testdatensatz der aus den ersten danch folgdnen percent_test (z.B. 15 von 100) Zeilen von not_shuffle besteht
#      validation:   Validierungsdatensatz der aus den restlichen Zeilen (z.B. 25 von 100) von not_shuffle besteht
#   Diese Funktion ist (wie man leicht sieht) aus der Funktion split_matrix_horizontal_randomly()
#   entstanden indem das Mischen einfach weg gelassen wurde.
def split_matrix_horizontal(X, y, percent_train=70, percent_test=15):

  not_shuffeld = np.hstack((X,y))
  percent_validation = 100 - percent_train - percent_test

  cols_X = X.shape[1]
  rows = X.shape[0]
 
  
  end_training = int(rows*percent_train/100)
  end_testing = end_training + int((rows * percent_test/100))
  
  X_training = not_shuffeld[: end_training  , :cols_X]          
  X_testing = not_shuffeld[end_training:end_testing  , :cols_X]
  X_validation = not_shuffeld[end_testing:  , :cols_X]

  y_training = not_shuffeld[:end_training , cols_X:]            
  y_testing = not_shuffeld[end_training:end_testing , cols_X:]
  y_validation = not_shuffeld[end_testing:  , cols_X:]
  
  return X_training, X_testing, X_validation, y_training, y_testing, y_validation



##################################################################################################
#          Matrix mit nur einer Spalte (n,1) in ein eindimensionale Feld (n,) umwandeln          #
##################################################################################################
def matrix_to_onedimensional_array(X):
  if X.ndim > 1:        
      
    X = np.reshape(X,[X.size, ])

  return X

##################################################################################################
#        Eindimensionales Feld (n,) in eine Matrix mit nur einer Spalte (n,1) umwandeln           #
##################################################################################################
def onedimensional_array_to_onedimensional_matrix(X):
    if X.ndim == 1:           
        X = np.reshape(X,[X.size,1])

    return X

##################################################################################################
#             Eine Spalte mit Einsen rechts an eine Matrix anhaengen                            #
##################################################################################################
def append_bias_column(X):
    
    onedimensional_array_to_onedimensional_matrix(X)
    
    xrows, xcols=X.shape
    
    fix_ones = np.ones(xrows)
    fix_ones = np.reshape(fix_ones,[fix_ones.size,1])        # In einspaltige Matrix umwnandeln
    
    
    X = np.hstack((X, fix_ones))

    return X
    

##################################################################################################
#                                                                                                #
#                                         Matrix-Analyse                                          #
#                                                                                                #
##################################################################################################


##################################################################################################
#                       Kovarianz zwischen SPALTEN einer Matrix                                 #
##################################################################################################
#      Berechnet die Kovarianz zwischen den SPALTEN einer Matrix.
#      D.h man geht hier davon aus, dass ein Merkmalsvektor eine SPALTE darstellt und somit
#      in einer Spalte die Verteilung eines einzigen Merkmals enthalten ist.
#      Es werden dann die Kovarianzen zwischen diesen SPALTEN (Merkmalen) berechnet.

def covariance_matrix(X):
    X_covariance = np.cov(X.T)  # Kovarianzmatrix berechnen
                                # ACHTUNG: np.cov() berechnet die Kovarianz zwischen den ZEILEN einer Matrix.
                                # D.h man geht bei np.cov() davon aus, dass ein Merkmalsvektor ein ZEILE 
                                # darstellt und somit in einer Zeile die Verteilung eines einzigen Merkmals 
                                # entahlten ist.
                                # In Pandas und auch bei uns in der ML-Lib stehen die Werte 
                                # eines einzige Merkmals jedoch in einer Spalte.
                                # Daher uebergeben wir an np.cov() die Transponierte von X (also X.T)
    
    return X_covariance



##################################################################################################
#                       Korrelation zwischen SPALTEN einer Matrix                                 #
##################################################################################################
#      Berechnet die Korrelation zwischen den SPALTEN einer Matrix.
#      D.h man geht hier davon aus, dass ein Merkmalsvektor eine SPALTE darstellt und somit
#      in einer Spalte die Verteilung eines einzigen Merkmals enthalten ist.
#      Es werden dann die Korrelationen zwischen diesen SPALTEN (Merkmalen) berechnet.
#      Das hier verwendete Verfahren ist numerisch deutlich besser konditioniert
#      als das naheliegende Verfahren, bei dem zunaechst die Matrix spaltenweise standadisiert
#      und danach deren spaltenweise Korrelationen berechnet werden.

def correlation_matrix(X):
    X_covariance = np.cov(X.T)  # Kovarianzmatrix berechnen
                                # ACHTUNG: np.cov() berechnet die Kovarianz zwischen den ZEILEN einer Matrix.
                                # D.h man geht bei np.cov() davon aus, dass ein Merkmalsvektor ein ZEILE 
                                # darstellt und somit in einer Zeile die Verteilung eines einzigen Merkmals 
                                # entahlten ist.
                                # In Pandas und auch bei uns in der ML-Lib stehen die Werte 
                                # eines einzige Merkmals jedoch in einer Spalte.
                                # Daher uebergeben wir an np.cov() die Transponierte von X (also X.T)
    
    v = np.sqrt(np.diag(X_covariance))      # Extrahieren der Diagonalen der Kovarianzmatrix als Vektor und ziehen der Quadratwurzel bei allen Vektorkomponenten
    outer_v = np.outer(v, v)                # Das auessere Produkt des Vektors mit sich selbst bilden => Matrix outer_v
    X_correlation = X_covariance / outer_v  # Die Matrizen dividieren
    return X_correlation


##################################################################################################
#                    Funktionswert aus je einer Spalte einer Matrix                              #
##################################################################################################
#      Je Spalte einer Matrix wird auf Basis der Funktion func ein Ergebniswert 
#      (z.B. der Mittelwert mit func=mean_of_matrix) berechnet und in der einzeiligen 
#       Ergebnismatrix zurueckgeben.
def get_functionvalue_of_matrix_columns(X,func):

  X = onedimensional_array_to_onedimensional_matrix(X)  
  
  X_size_rows, X_size_cols = X.shape              
  einspaltige_matrix = np.zeros((X_size_rows,1))  
                                                  
                                                  
  funtionvalue_row = np.zeros((1,X_size_cols))      
  
  # Alle Spalten der Matrix X nacheiander EINZELN standardisieren, denn es macht keinen Sinne 
  # ueber mehrere Merkmale (z.B. Einkommen und Alter) gleichzeitig zu standardisieren
  for i in range(X_size_cols):
    einspaltige_matrix[ : , 0] = X[ : , i]   # Die i-te Spalte von X in die einspaltge Matrix kopieren 
   
    funtionvalue_row[0][i] = func(einspaltige_matrix)
    
  return funtionvalue_row


##################################################################################################
#                                                                                                #
#                                 Matrix-Manipulationen                                          #
#                                                                                                #
##################################################################################################

##################################################################################################
#                            Skalierung der Spalten einer Matrix                                 #
##################################################################################################
#      Skalierung der Spalten einer Matrix mit jeweils spalteneigenen Parametern
#      Dabei wird jede Spalte der Matrix auf Basis die EINZEILIGEN Matrizen mean und variance 
#      wie folgt standardisiert:
#         out = (in - mean)/standard-deviation
#      Die Parametername dieser Funktion sind deshalb so gewaehlt weil diese Funktion typischerweise
#      fuer eine Standadisierung eingestzt wird. 
def scale_of_matrix_columns(X, mean, standard_deviation):

  X = onedimensional_array_to_onedimensional_matrix(X)  
  
  X_size_rows, X_size_cols = X.shape              
  einspaltige_matrix = np.zeros((X_size_rows,1))  
  
  
  Xstd = np.zeros((X_size_rows,X_size_cols))     

  # Alle Spalten der Matrix X nacheiander EINZELN skalieren.
  for i in range(X_size_cols):
    einspaltige_matrix[ : , 0] = X[ : , i]                   # Die i-te Spalte von X in die einspaltge Matrix kopieren 
   
    std_einspaltige_matrix = (einspaltige_matrix - mean[0,i]) / standard_deviation[0,i]

    Xstd[ : , i] = std_einspaltige_matrix[ : , 0]           # Die standardisierten Werte dann in die Ergebnismatrix schreiben

  return Xstd

##################################################################################################
#                      Standardisierung der Spalten einer Matrix                                 #
##################################################################################################
def standardisation_of_matrix_columns(X):
    
  X = onedimensional_array_to_onedimensional_matrix(X)  

  means_per_col                = get_functionvalue_of_matrix_columns(X, np.mean)
  standard_deviations_per_col  = get_functionvalue_of_matrix_columns(X, np.std)

  X_std = scale_of_matrix_columns(X, means_per_col, standard_deviations_per_col)
  
  return X_std, means_per_col, standard_deviations_per_col

##################################################################################################
#                      Re-Standardisierung der Spalten einer Matrix                              #
##################################################################################################
def restandardisation_of_matrix_columns(X, means_per_col, standard_deviations_per_col ):
    
  X = onedimensional_array_to_onedimensional_matrix(X)  

  # Die Hintransformation erfolgte mit folgender Skalierung:  
  #   Out = (In - Mittelwert) / Standardabweichung
  # Die Rücktransformation kann somit wie folgt durchgeführt werden: 
  #   Out = (In * Standardabweichung) + Mittelwert 
  # Um die selbe Funktion "scale_of_matrix_columns()"" wie für die Hintransformation nutzen zu können, 
  # kann man die obige Glechung wie folgt umformen: 
  #   Out = (In - (- (Mittelwert / Standardabweichung) / (1/Standardabweichung)
  # Genauso machen wir das hier nun

  X = scale_of_matrix_columns(X, -(means_per_col/standard_deviations_per_col), (1/standard_deviations_per_col))

  return X

##################################################################################################
#                                                                                                #
#                           Lineare Regression mit Inverser Matrix                               #
#                                                                                                #
##################################################################################################
def linear_regression (X,y):

  # Die Matrix für XT_X anlegen. Diese ist quadratisch und hat soviele Zeilen/Spalten wie der 
  # Merkmalsvektor gross ist.
  dummy, groesse_merkmalsvektor = X.shape
  
  XT_X = np.zeros((groesse_merkmalsvektor,groesse_merkmalsvektor))

  # Die Matrix XT_X berechnen
  XT_X=np.dot(X.T, X)

  # Die Inverse zu XT_X berechnen
  XT_X_inv = np.linalg.inv(XT_X)

  # Den Zielwertvektor mit der Matrix X.T multiplizieren
  XT_y = np.dot(X.T, y)

  w = np.dot(XT_X_inv, XT_y )

  return w



##################################################################################################
#                                                                                                #
#                                 Test- und Validierungs-Metriken                                #
#                                                                                                #
##################################################################################################

##################################################################################################
#                  Berechnung des Fehlerquadrats (Mean-Square-Error, MSE)                        #
##################################################################################################
def mean_square_error_of_two_matrixes(soll, ist):
    
    error = soll - ist          # Differenzmatrix
    
    error_2 = error * error     # Jedes Matrixelement quadrieren
    
    sum = np.sum(error_2)       # Summe ueber alle Matrixelement
    
    return sum/np.size(error_2) # Durch die Anzahl der Feldelement dividieren
 
# Nur damit man einmal sieht welche Vorteile das Rechnen mit Matrizen mit sich bringt ...
def mean_square_error_of_two_matrixes_conventional(soll, ist):  # Wie man es in C oder Java programmieren wuerde
  error = 0.0

  rows, cols = soll.shape           
  count = rows * cols


  for i in range(rows):
    for j in range(cols): 
      error += (soll[i][j] - ist[i][j]) ** 2
  
  error=error/count
  return error 



######## BIS HIERHIN SOLL DIE ML-LIB IM RAHMEN DAS TUTORIALS 1 VERSTANDEN WERDEN !!!   ##########
























##################################################################################################
#                                                                                                #
#                              Gradientenabstiegsverfahren (Neuron)                              #
#                                                                                                #
##################################################################################################

##################################################################################################
#                                      Ausgabe berechnen                                         #
##################################################################################################
def calculate_output_of_neuron(X, coefficients, loss_actfunc_type):

  # ACHTUNG:     Vout = np.dot(X,coefficients)     ...
  # ... ist dann mehrdeutig wenn X und coefficients nicht beide vom Typ Matrix sind.
  # Damit diesbezueglich Klarheit herrscht, werden eindimensionale Felder (n,) 
  # hier in einspaltige Matrizen (n,1) umgewandelt.
  # Ein Merkmalsvektor steht also in einer Zeile von X ???
  # coefficient ist also eine Matrix mit nur einer ??? 
  coefficients = onedimensional_array_to_onedimensional_matrix(coefficients)
  
  net = np.dot(X, coefficients)      # net ist damit ein Matrix mit nur einer Spalte
  predictions = net + 0              # Nur einfach zuweisen damit ein array der richtigen Groesse 
                                     # schon mal angelegt wird. Die Werte von prediction werden 
                                     # dann weiter unten noch ueberschrieben.

  if loss_actfunc_type == "MSE_LOGISTIC" :
    predictions = 1. / (1.+np.exp(-net))  # erzeugt für jedes Vektorelemente von net, das entsprechende Vektorelement von prediction

  if loss_actfunc_type == "MSE_LINEAR" :
    predictions = net

  if loss_actfunc_type == "MSE_LINEAR_BEGRENZT" :
    for iter in range(len(net)):
      if net[iter] < 0:
        predictions[iter] = 0
      else:
        if net[iter] > 1:
          predictions[iter] = 1
        else :
          predictions[iter] = net[iter]
       
  if loss_actfunc_type == "CEC" :                               # Aktivierungsfunktion fuer den Fall Cross-Entropy-Cost-Function
    predictions = 1. / (1.+np.exp(-net))                        # Da in der ML-Lib nur ein einziges Neuron simuliert werden kann, ist eine
                                                                # Softmax-Funktion nicht implementierbar, denn dafür benoetigt man wenigsten zwei Neuronen.
                                                                # Daher wird hier die folgenden Naehrung angenommen:
                                                                #       Wir haben ein zweites "gedachtes" aber nicht simuliertes Neuron und dessen Output ist Null !!!!
                                                                #       Für diese Naeherung erhalten wir dann naemlich die Sigmoide (alias logistische) Funktion als  Aktivierungsfunktion
                                                                
    #predictions = np.exp(net) / (1 + np.exp(net))              # Dies waere das Aequivalent zu der obigen Formel
    #predictions = np.exp(net) / np.sum(np.exp(net))            # Dies stellt zwar rein optisch die Softmax-Funktion dar, doch unter Kenntnis der Inhalte von net
                                                                # ist diese Berechnung absolut hirnrissig, denn die Summe geht ueber die net-Ausgaben des 
                                                                # einen einzigen simulierten Neurons wenn man nacheinander die Trainingsdaten anlegt.
                                                                # Die Summe muss aber ueber alle Neuronen des einen Layers gehen (siehe Skript Softmax-Aktivierungsfunkton)
  return predictions, net


##################################################################################################
#                                  Berechnung von delta_w                                        #
##################################################################################################
def calculate_delta_w_of_neuron(X, Ypred, Y, loss_actfunc_type): 
 
    # Um den Bezug zu Skript herzustellen, erzeugen wir Variablen mit den dort verwendeten Bezeichnungen und weisen
    # denen die entsprechende Werte zu:
    # ACHTUNG: Solche "Zuweisungen" erzeugen keine neuen Arrays sondern nur ALIAS-Namen, die dann auf dem selben Datenbestand arbeiten
    # X= X          # Eingabe
    out   = Ypred   # Ausgabe (Ist)
    soll  = Y       # Ausgabe (Soll)

    # Hier muss unterschieden werden ob wir mit dem
    #   A.) Mean-Square-Error-Fehlermaß  (MSE) in Kombination mit der Logistischen-Aktivierungsfunktion    oder mit dem
    #   B.) Cross-Entropy-Fehlermass in Verbindung mit der Softmax-Aktivierungsfunktion (die bei nur zwei Ausgaengen ja der Logistischen-Aktivierungsfunktion entspricht)
    # arbeiten wollen. 
    #
    # Im Fall A gilt;
    #   delta_w = (soll - out) * f'(net) * X     mit w_neu = w_alt +  delta_w * Ungeduldigkeitsfaktor
    #   mit f = logistischer Funktion gilt dann     f'(net) = out * (1 - out)  . Hierbei ist out nur eine andere Bezeichnung fuer Ypred_i
    #   Folglich gilt:         delta_w = (soll - out) * out * (1 - out) * X
    #
    # Im Fall B gilt:          delta_w = (soll - out)                   * X 


 
    # ACHTUNG:     Vout = np.dot(X,Y)     ...
    # ... ist dann mehrdeutig wenn X und Y nicht beide vom Typ Matrix sind.
    # Damit diesbezueglich Klarheit herrscht, werden eindimensionale Felder (n,) hier in einspaltige Matrizen (n,1) umgewandelt.
    out  = onedimensional_array_to_onedimensional_matrix(out)
    soll = onedimensional_array_to_onedimensional_matrix(soll)
 

    errors = soll - out # Differenz(-vektor) zwischen soll und ist berechnen

    if loss_actfunc_type == "MSE_LOGISTIC":
      f_strich = out * (1 - out)
      temp = errors * f_strich
      delta_w = np.dot(X, temp)

    if loss_actfunc_type == "MSE_LINEAR":
      f_strich = 1
      temp = errors * f_strich
      delta_w = np.dot(X, temp)  

    if loss_actfunc_type == "MSE_LINEAR_BEGRENZT":
      f_strich = 1
      temp = errors * f_strich
      delta_w = np.dot(X, temp) 

    if loss_actfunc_type == "CEC":
      delta_w = np.dot(X, errors)/len(X)    # X_i * error_i berechnen. 
                                            # Im Skript ist 
                                            #       errors_i mit delta_i=(o_i - s_i) 
                                            #       delta_w mit dem griechischen delt gefolgt von w 
                                            #       len(X) mit Mini-Batch-Size N
                                            # benannt.
                                            

    return delta_w, errors


##################################################################################################
#                               Den Loss (Verlust)  berechnen                                    #
##################################################################################################
def calculate_loss_of_neuron(X, y, coefficients, loss_actfunc_type):


    
    # ACHTUNG:     Vout = np.dot(X,Y)     ...
    # ... ist dann mehrdeutig wenn X und Y nicht beide vom Typ Matrix sind.
    # Damit diesbezueglich Klarheit herrscht, werden eindimensionale Felder (n,) hier in einspaltige Matrizen (n,1) umgewandelt.
    y  = onedimensional_array_to_onedimensional_matrix(y)
    coefficients = onedimensional_array_to_onedimensional_matrix(coefficients)

    if loss_actfunc_type == "MSE_LOGISTIC":
      y_pred,net = calculate_output_of_neuron(X, coefficients,loss_actfunc_type )
      d = y - y_pred 
      lp = np.sum( (d * d) ) / (2*len(X)) # Der Faktor 1/2 wird in der Regel weggelassen und bei der Lernrate beruecksichtigt
      #lp = np.sum( (d * d) ) / (len(X))   # Ohne den Faktor 1/2 berechnen wir den Mean Square Error (MSE)   
      

    if loss_actfunc_type == "MSE_LINEAR":
      y_pred, net = calculate_output_of_neuron(X, coefficients,loss_actfunc_type )
      d = y - y_pred 
      lp = np.sum( (d * d) ) / (2*len(X)) # Der Faktor 1/2 wird in der Regel weggelassen und bei der Lernrate beruecksichtigt
      #lp = np.sum( (d * d) ) / (len(X))   # Ohne den Faktor 1/2 berechnen wir den Mean Square Error (MSE)   


    if loss_actfunc_type == "MSE_LINEAR_BEGRENZT":
      y_pred,net = calculate_output_of_neuron(X, coefficients,loss_actfunc_type )
      d = y - y_pred 
      lp = np.sum( (d * d) ) / (2*len(X)) # Der Faktor 1/2 wird in der Regel weggelassen und bei der Lernrate beruecksichtigt
      #lp = np.sum( (d * d) ) / (len(X))   # Ohne den Faktor 1/2 berechnen wir den Mean Square Error (MSE)   


    # Dies ist die eigentliche CEC. Diese fuehrt aber zu numerischen Instabilitaeten..
    if loss_actfunc_type == "CEC":
      y_pred,net = calculate_output_of_neuron(X, coefficients,loss_actfunc_type )
      d = y - y_pred      # Nur damit d einfach schon mal angelegt ist. Es wird spaeter noch ueberschrieben


      # Um log(0)=UNENDLICH zu vermeiden was natürlich durch das gefolgte * 0 aufgehoben wuerde, 
      # wird hier eine Fallunterscheidung fuer jede einzelne Komponente des Sollausgabevektor (y) gemacht
      # Bezug zum Skript:
      #     y entspricht im Skript dem s
      #     y_pred entspricht im Skript dem o
      #     len(X) entspricht im Skript dem N
      #     Da wir neu ein einziges Neuron simulieren ist das M aus dem Skript hier nicht zu finden 
      
      for k in range(len(y)):
        if y[k] == 0.0:                    
          d[k] =  np.log(1-y_pred[k])      
        else :
          d[k] =  np.log(y_pred[k])

      lp = -np.sum( d ) / (len(X))          

    # ... daher nimmt man typischerweise die folgende Average-Log-Likelihood-Kostenfunktion die zu den selbem Ergebnissen wie
    # die CEC fuehrt aber numerisch deutlich stabiler ist
    if loss_actfunc_type == "CEC_ALL":
      y_pred,net = calculate_output_of_neuron(X, coefficients,loss_actfunc_type )
      d = y - y_pred
      
      indicator = (y==+1)
      scores = np.dot(X, coefficients)
      logexp = np.log(1. + np.exp(-scores))
    
      # Ueberpruefe und verhindere Overflow
      mask = np.isinf(logexp)
      logexp[mask] = -scores[mask]
    
      lp = -np.sum((indicator-1)*scores - logexp)/len(X)
      

    return lp, y_pred, d


##################################################################################################
#                     Das Gradientenabstiegsverfahren durchfuehren                               #
##################################################################################################
def gradient_descent_of_neuron(X_train, y_train, X_test, y_test, initial_weights, step_size, batch_size, max_iter, loss_actfunc_type, do_shuffle, trace_level, do_epoche_log, do_iteration_log):
 
    # Datenstrukturen fuer das Loggen von werten anlegen
    log_epoche_loss_train = []
    log_epoche_loss_test = []
    log_epoche_coefficients = np.zeros( ( 1,   len(initial_weights)  )  ) 

    log_iteration_loss_train = []
    log_iteration_loss_test = []
    log_iteration_coefficients = np.zeros( (  1,  len(initial_weights)  )  )
    
  
    epoche = 0
    
    LOG_EPOCHE = do_epoche_log
    LOG_ITERATION = do_iteration_log
    
    # Koeffizienten (Gewichte) initialisieren 
    # ACHTUNG:     Vout = np.dot(X,Y)     ...
    # ... ist dann mehrdeutig wenn X und Y nicht beide vom Typ Matrix sind.
    # Damit diesbezueglich Klarheit herrscht, werden eindimensionale Felder (n,) hier in einspaltige Matrizen (n,1) umgewandelt.
    coefficients = onedimensional_array_to_onedimensional_matrix(initial_weights)

    if LOG_EPOCHE == 1 :
        # Verifizieren wie gut die initialen Koeffizienten fuer eine Epoche waeren                  
        # Den Verlust ueber alle Daten berechnen
        lp_epoche_train, pred_epoche_train, err_epoche_train = calculate_loss_of_neuron(X_train, y_train, coefficients, loss_actfunc_type)
        lp_epoche_test,  pred_epoche_test,  err_epoche_test  = calculate_loss_of_neuron(X_test, y_test, coefficients, loss_actfunc_type)

        # Die berechneten Werte an die Aufzeichung zu den Epochen-Loss anhaengen
        log_epoche_loss_train.append(lp_epoche_train)
        log_epoche_loss_test.append(lp_epoche_test)

        # Die Gewichte an die Aufzeichnung der Gewichte "anhaengen", bzw. an der 0.-ten Zeile eitragen  
        log_epoche_coefficients[0:] = coefficients.transpose() + 0 # Zugriff auf die 0-te Zeile 
    # <-- LOG_EPOCHE

    if LOG_ITERATION == 1 :
        # Verifizieren wie gut die initialen Koeffizienten fuer eine Iteration waeren                  
        # Den Verlust ueber alle Daten berechnen
        lp_iteration_train, pred_iteration_train, err_iteration_train = calculate_loss_of_neuron(X_train, y_train, coefficients, loss_actfunc_type)
        lp_iteration_test,  pred_iteration_test,  err_iteration_test  = calculate_loss_of_neuron(X_test, y_test, coefficients, loss_actfunc_type)

        # Die berechneten Werte an die Aufzeichung zu den Iteration-Loss anhaengen
        log_iteration_loss_train.append(lp_iteration_train)
        log_iteration_loss_test.append(lp_iteration_test)

        # Die Gewichte an die Aufzeichnung der Gewichte "anhaengen", bzw. an der 0.-ten Zeile eitragen  
        log_iteration_coefficients[0:] = coefficients.transpose() + 0 # Zugriff auf die 0-te Spalte
    # <-- LOG_ITERATION

    
    

    
    ## Initiales Mischen der Trainingsdatenreihe                                              
    if do_shuffle == 1 :                             # do_shuffle == 1: Trainingsdaten werden je Batch gemischt, ansonsten nicht
      np.random.seed(seed=1)                         # Die Zufallsgenerator initialisieren
      permutation = np.random.permutation(len(X_train))    # Eine zufaellige Indizierung durch Permutation der Indizes erzeugen
      X_train = X_train[permutation,:]                           # Die zufaellige Indizierung auf die Eingabedaten anwenden UND
      y_train = y_train[permutation]                             # ... die selbe zufaellige Indizierung auch auf die Ausgabedaten anwenden.


    i = 0 # i ist quasi ein Zeiger (Index) der auf das erste Datenpaar (X,y) des Batch-Blocks
          # (also der Daten die in einem Minibatch verarbeitet werden) zeigt.
          # Bearbeitet werden immer die Daten in der (Trainings-)Datenreiehe von der Zeile i bis zur Zeile i+batch_size-1


    # Der Parameter max-iter bestimmt die Anzahl der Mini-Batches (also der Anzahl der Gewichtsanpassungen) die zu durchlaufen sind.
    # Dies ist unabhaengig von der Groesse der Trainingsdatenreihe
    # Ist z.B. der Wert von max_iter * batch_size  keiner als die Anzahl der Daten in der Trainingsdatenreihe
    # dann werden nicht alle Trainingsdaten "durchlaufen", sondern das Training vorher abgebrochen.
    for itr in range(max_iter):

        # Die Ist-Ausgabe (alias prediction) erzeugen
        # ... und zwar anhand der Trainings-Eingabedaten von i bis i+batch_size und der aktuellen Koeffizienten
        out, net = calculate_output_of_neuron(X_train[i: i + batch_size,:], coefficients, loss_actfunc_type)
        
        # Die Soll-Ausgabe (alias indicator) definieren ...
        # ... und zwar anhand der Trainings-Soll-Ausgabedaten y von i bis i+batch_size 
        soll = y_train[i:i+batch_size]           

        # Alle Koeffizienten anpassen
        for j in range(len(coefficients)): 
            delta_w_minibatch,errors_trace = calculate_delta_w_of_neuron(X_train[i:i+batch_size,j], out, soll, loss_actfunc_type)           
            coefficients[j] = coefficients[j]  + ( (delta_w_minibatch / float(batch_size))   * step_size ) # step_size ist unsere Lernrate (alias Ungeduldigkeistfaktor)

        if LOG_ITERATION == 1 :
            # Verifizieren wie gut die soeben neu berechneten Koeffizienten fuer eine Epoche waeren                  
            # Den Verlust ueber alle Daten berechnen
            lp_iteration_train, pred_iteration_train, err_iteration_train = calculate_loss_of_neuron(X_train, y_train, coefficients, loss_actfunc_type)
            lp_iteration_test,  pred_iteration_test,  err_iteration_test  = calculate_loss_of_neuron(X_test, y_test, coefficients, loss_actfunc_type)

            # Die berechneten Werte an die Aufzeichung zu den Iteration-Loss anhaengen
            log_iteration_loss_train.append(lp_iteration_train)
            log_iteration_loss_test.append(lp_iteration_test)

            # Die Gewichte an die Aufzeichnung der Gewichte "anhaengen", bzw. an der 0.-ten Zeile eitragen  
            log_iteration_coefficients =  np.vstack((log_iteration_coefficients, coefficients.transpose()  ))
        # <-- LOG_ITERATION

        if trace_level >= 10:
            soll_minus_out = soll-out
            print("\nLoss_Activation-unction:", loss_actfunc_type)
            print("Iteration:    %s" % str (itr) )
            print("Eingabe:      %s" % str (X_train[i: i + batch_size,:]) )
            print("Netzeingabe:  %s" % net )
            print("Vorhersage:   %s" % str (out) )
            print("Soll-Ausgabe: %s" % str (soll) )
            print("Fehler:       %s" % str (soll_minus_out)) 
            print("errors_trace  %s" % str (errors_trace))
            print("delta_w:      %s" % str (delta_w_minibatch) )
            print("Gewichte:     %s" % str (coefficients) ) 

  

        i += batch_size   # Den Zeiger i so weiter "nach vorne verbiegen", dass dieser auf 
                          # den Anfang des naechsten Batch-Block zeigt
 
        # Eine EPOCHE IST "VORBEI", es sind also alle Trainigsdaten einmal durchlaufen (mit welcher Batch-Size auch immer).
        # Dies ist daran erkennbar, dass der naechste Batch-Block ausserhalb der Datenreihe laege. Wenn dem so ist, dann
        #     1.) Verifizieren wie gut die Vorhersage nach der letzten Epoche waeren
        #     2.) Die Datenreihe neu mischen
        #     3.) wieder vorne in der neu gemischten Datenreihe anfangen
        if i+batch_size > len(X_train):
            
            if LOG_EPOCHE == 1 :
                # 1.) Verifizieren wie gut die Vorhersage nach der letzten Epoche waeren                  
                # Den Verlust ueber alle Daten berechnen
                lp_epoche_train, pred_epoche_train, err_epoche_train = calculate_loss_of_neuron(X_train, y_train, coefficients, loss_actfunc_type)
                lp_epoche_test,  pred_epoche_test,  err_epoche_test  = calculate_loss_of_neuron(X_test, y_test, coefficients, loss_actfunc_type)

                # Den berechneten Wert an die Aufzeichung zum Epochen-Loss anhaengen
                log_epoche_loss_train.append(lp_epoche_train)
                log_epoche_loss_test.append(lp_epoche_test) 

                # Die Gewichte an die Aufzeichnung der Gewichte anhaengen
                # Im Gegensatz zum log_loss werden hier Arrays angehaengt
                log_epoche_coefficients =  np.vstack((log_epoche_coefficients, coefficients.transpose()))

            if trace_level >= 1 : 
                # Eckwerte des aktuellen Mini-Batch (insbesondere auch den aktuellen Log-Likelihood) ausgeben  
                print ("\n-------------------------------------------------------------------------------------")      
                print ("NACH Epoche: %d loss: %.8f pred: %s err: %s" % (epoche, lp_epoche_train, str(pred_epoche_train), str(err_epoche_train) ) )   
                print ("-------------------------------------------------------------------------------------")
            

            # 2.) Die Datenreihe neu mischen
            if do_shuffle == 1 :
              permutation = np.random.permutation(len(X_train))  
              X_train = X_train[permutation,:]
              y_train = y_train[permutation]

            # 3.) wieder vorne in der gemischten Datenreihe anfangen
            i = 0
            epoche = epoche + 1
    # <--- for itr in xrange(max_iter):
                
    # Die berechneten Gewichte und die Liste mit den augezeichneten Fehlermassen zurueck geben
    return  coefficients, \
            log_epoche_loss_train,    log_epoche_loss_test,    log_epoche_coefficients, \
            log_iteration_loss_train, log_iteration_loss_test, log_iteration_coefficients
 
 
            
##################################################################################################
#                                                                                                #
#                                     Plotten                                                    #
#                                                                                                #
##################################################################################################

##################################################################################################
#                         Erzeugen eines Scatter-Matrix-Plots                                    #
##################################################################################################
def scatterplot_matrix(data, names, **kwargs):
    # ACHTUNG: Hier wird jede ZEILE von Data gegen jede andere ZEILE von Data geplottet.
 
    data = data.T # Die Matrix transponieren damit wir kompatibel zum Pandas-Scatter-Matrix-Plot sind
                  # denn bei dem werden die Spalten gegenüber allen anderen Spalten aufgetragen

    # Anzahl der Subplots bestimmen
    numvars, numdata = data.shape 
    fig, axes = plt.subplots(nrows=numvars, ncols=numvars, figsize=(16,16))
    fig.subplots_adjust(hspace=0.4, wspace=0.4)

    
    # Alle Diagramme in der Diagonalen ausblenden ....
    for i in range(numvars):
        axes[i,i].xaxis.set_visible(False)
        axes[i,i].yaxis.set_visible(False)
  
    # ... und statt dessen in der Diagonale die Namen (names) ausgeben
    for i, label in enumerate(names):
        # Die in label uebergebenen Namen in eine Annotation-Box ausgeben
        # die ihre Position in der Mitte (0.5,0.5) der Plot-Box hat.
        # Alles was in der Annotatiosbox geschrieben wird hat
        # bezueglich der Annotations-Box eine zentrierte (center) 
        # Ausrichtung und zwar horizontal (ha) als auch vertikal (va).
        axes[i,i].annotate(label, (0.5, 0.5), xycoords='axes fraction',
                ha='center', va='center')  
        
        # Das folgende <-----> unten in der Box soll andeuten, dass
        # in dieser Zeile der Plot-Matrix die horizontale Achse aller
        # Plots diese Zeile durch den darueber dargestellten Namen
        # bestimmt wird
        axes[i,i].annotate("<------>", (0.5, 0), xycoords='axes fraction',
                ha='center', va='bottom')

    # Die eigentlichen Daten plotten
    for i, j in zip(*np.triu_indices_from(axes, k=1)):
        for x, y in [(i,j), (j,i)]:
            axes[x,y].plot(data[x], data[y], **kwargs)

    return fig


##################################################################################################
#                                Einen Heatmap-Plott erzeugen                                    #
##################################################################################################
def show_matrix_as_heatplot(mat_show, rows_print, cols_print, font_size=24):
  # Ausgabe der Matrix als Heatmap ohne Beschriftung
  fig, ax = plt.subplots(figsize=(10,10),)
  im = ax.imshow(mat_show)

  # Die Frablegende erstellen
  cbar = ax.figure.colorbar(im, ax=ax)
  cbar.ax.set_ylabel('', rotation=-90, va="bottom")

  # Die Achsenbeschriftung
  ax.set_xticks(np.arange(len(cols_print)))
  ax.set_xticklabels(cols_print)

  ax.set_yticks(np.arange(len(rows_print)))
  ax.set_yticklabels(rows_print)

  # Rotation der Beschriftung
  plt.setp(ax.get_xticklabels(), rotation=45, ha="right",rotation_mode="anchor")

  # Die Beschiftung jeder Kachel der Heatmap
  for i in range(len(rows_print)):
    for j in range(len(cols_print)):
        text = ax.text(j, i, "{:4.2f}".format(mat_show[i, j]), 
                       #ha="center", va="center", color='grey' ,fontsize=24)
                       ha="center", va="center", color='grey' ,fontsize=font_size)
                       
  # Ggf.nach dem Ausdruck noch eine ueberschrift mitgeben      
  # plt.title('Kovarianzmatrix')

  plt.show()
  return
  
  
##################################################################################################
#                                Textuelle Histogrammausgabe                                     #
################################################################################################## 
def histogram_as_text(X, unterer_grenzwert = 0, oberer_grenzwert = 100, anzahl_balken = 100, dezimalstellen_fuer_grenzwerteausgabe=2):
  
  # Das Histogramm erstellen
  anzahl_je_wert, werte = np.histogram(X,range=(unterer_grenzwert, oberer_grenzwert), bins=anzahl_balken)

  werte = np.around(werte, decimals=dezimalstellen_fuer_grenzwerteausgabe)  # Auf n Dezimalstelle begrenzen damit die 
                                                                            # Tabelle schoen spaltenmaessig ausgegeben wird

  for i in range(anzahl_je_wert.shape[0]):  # Die Texttabelle ausgeben
    with np.printoptions(precision=dezimalstellen_fuer_grenzwerteausgabe, suppress=True):
        print(werte[i],anzahl_je_wert[i])

  return
  
  
  
##################################################################################################
#                       Matrix mit Zeilen- und Spaltenbeschriftungen ausgeben                    #
##################################################################################################  
# Funktion zum Anzeigen einer Matrix mit beschrifteten Zeilen und Spalten
def print_labeled_matrix(cm, label_names):
    #Kopfzeile erstellen
    lines = []
    header = ("|{:^11}|"*(len(label_names)+1)).format('', *label_names)
    line_len = len(header)
    lines.append("-"*line_len)
    lines.append(header)
    lines.append("-"*line_len)

    hit = 0
    total = 0
    #Restliche Zeilen erstellen und füllen
    for i, row in enumerate(cm):
        hit += row[i]
        total += sum(row)
        lines.append(("|{:^11}|"*(len(label_names)+1)).format(label_names[i],
                                                                   *row))
        lines.append("-"*line_len)
    #Anzeigen
    print('\n'.join(lines))  


##################################################################################################
#                                Confusion-Matrix berechnen und anzeige                          #
################################################################################################## 

#Funktion zum erstellen der Confusion Matrix
def calculate_confusion_matrix(model, X_test, y_test, label_names, labels):
  #Erstellen der Confusion Matrix gefuellt mit 0
  cm = np.zeros((len(labels),len(labels)), dtype=int)

  #Das Netzwerk vorhersagen auf dem Testdatensatz treffen lassen und die 
  #vorhergesagte Klassennummer in der Variable Predict ablegen.
  predicted = model.predict(X_test)
  predicted = np.argmax(predicted,axis=1)

  #Umwandeln der Arrays in die entsprechende Nummer der zugehörigen Klasse
  y_test_labels_as_numbers = []
  for i in range(y_test.shape[0]):
    y_test_labels_as_numbers.append(np.where(y_test[i] == 1)[0])

  #Die vorhersage des Netzwerkes mit den tatsächlichen Werten vergleichen und in
  #cm eintragen.
  for i in range(len(y_test_labels_as_numbers)):
    cm[labels.index(y_test_labels_as_numbers[i])][labels.index(predicted[i])] += 1

  #Aufruf der Funktion zum anzeigen der confusion Matrix
  print_labeled_matrix(cm, label_names) 
  
  
##################################################################################################
#                      Klassifikationegrenzen im 2D-Merkmalsraum anzeigen                        #
################################################################################################## 
# Funktion, die den durch Merkmalsdaten abgedeckten Bereich des 2D-Merkmalsraum "gleichmaessig abscant" 
# und für jeden dieser Merkmalsvektoren die Klassifikation durchfuehrt und das Klassifikationsergebnis im Merkmalsraum darstellt.
def plot_seperation_levels_in_2D_feature_space(X_dataset_std, model, labels, titel="Trennungsraum", detailgrad_trennungsbereich=20, inputdata_names=[], eingebadaten_sind_bereits_standardisiert=True, X_dataset=0, x_means=0, x_standard_deviations=0):
  # Minima und Maxima der Daten ermitteln 
  x_achse_min = min(X_dataset_std[:,0])
  x_achse_max = max(X_dataset_std[:,0])
  y_achse_min = min(X_dataset_std[:,1])
  y_achse_max = max(X_dataset_std[:,1])


  # Schrittgroesse mit der der Merkmalsraum abgescant wird berechen
  steps = abs(x_achse_min-x_achse_max)/detailgrad_trennungsbereich

  # Zwei Felder (jeweils eines fuer x und eines fuer y) erstellen, die jeweils
  # alle abgescanten x bzw. y-Koordinaten fuer sich enthalten.
  # Diese sind dann quasi das Raster jeweils auf der x- bzw. auf der y-Achse
  all_steps_x = np.arange(x_achse_min, x_achse_max+steps, steps)
  all_steps_y = np.arange(y_achse_min, y_achse_max+steps, steps) 

  # Aus der Kombination der Raster auf der x- und auf der y-Achse
  # alle entsprechenden Punkte (xy-Koordinaten) im Merkmalsraum berechnen und in 
  # "train_fuer_darstellung" abspeichern. In "train_fuer_darstellung" stehen somit alle
  # abgescanten Punkte des Merkmalsraumes.
  train_fuer_darstellung = []
  for i in all_steps_x:
    for j in all_steps_y:
      train_fuer_darstellung.append([i,j])

  # Allen abgescanten Punkten des Merkmalsraumes dem Klassifikator vorlegen
  predicted = model.predict(train_fuer_darstellung) # Berechnung des Ausgabevektors

  predictedLabel = np.argmax(predicted,axis=1)      # Bestimmung des Index des Ausgabevektors
                                                    # an dem der maximale Wert im Ausgabevektor steht.
                                                    # Also Bestimmung des eigentlichen Klassifikationsergebnisses

  # Wenn nicht standartisierte Daten geplotted werden sollen, so muessen die means und standard_deviations 
  # berechnet werden um die Daten in den nicht standardisierten Merkmalsraum zurueck zu rechnen
  if eingebadaten_sind_bereits_standardisiert == False:
    if X_dataset.any() != 0  and x_means.any() != 0 and x_standard_deviations.any() !=0:
      train_fuer_darstellung = restandardisation_of_matrix_columns(np.array(train_fuer_darstellung),x_means,x_standard_deviations)
    else:
      print("Fehler! Es fehlt (wenigstens) eine Angabe zur Nutzung des nicht standartisierten Datensatzes!")

  
  # Eindeutige Farbewerte (Klassenfarbe) für jedes Label (jede Klasse) erstellen
  cmap = plt.cm.get_cmap('hsv', len(labels)+1)

  # Zeichnen der einzelnen Punkte in der entsprechenden Klassen-Farbe
  for counter, value in enumerate(train_fuer_darstellung):
    for label in labels:
      if predictedLabel[counter] == label:
        plt.scatter(value[0],value[1],color=cmap(label))

  plt.title(titel)
  # Zur Kontrolle auch Plotten des uebergebenen Datensatzes, also der Trainingsdaten
  if eingebadaten_sind_bereits_standardisiert == True:
    plt.plot(X_dataset_std[:,0], X_dataset_std[:,1], 'bo', alpha=0.3)
  else:
    plt.plot(X_dataset[:,0], X_dataset[:,1], 'bo', alpha=0.3)
  
  # Falls keine Beschriftungen an diese Funktion uebergeben wurden, dann die Beschriftung
  # einfach mit "0" bzw. "1" vorgeben
  if not inputdata_names:
    plt.xlabel("0")
    plt.ylabel("1")
  else:
    plt.xlabel(inputdata_names[0])
    plt.ylabel(inputdata_names[1])
  
  # Darstellung des Plots
  plt.show()
  
  
##################################################################################################
#                             Mehrer Bilder in Kachelanordnung ausgeben                          #
##################################################################################################   
# Die Parameter sind:
#
#       imgs_to_show:               Matrix mit den Bilddaten. Diese Matrix hat folgenden Aufbau:   
#                                   shape (Anzahl_Bilder, Anzahl_Zeilen , Anzahl_Spalten, Anzahl_Farbkanaele)
#       anzahl_kachelspalten:       Gibt an wieviele Bilder nebeneinander dargestellt werden sollen.
#       gesamtplotbreite_in_pixel:  Breite des Platzes der im Plot fuer die Darstellung bereit gestellt wird.
#                                   Mit einem Wert von 2400 Pixeln ist man in der Regel etwas breiter als der
#                                   Monitor. Dies ist nicht kritisch, denn matplot skaliert den Gesamtplot dann
#                                   auf den zur Verfuegung stehenden Platz herunter, jedoch nicht herauf.
#       hide_axes=False:            True => Die Koordinatenachsen werden mit ausgegeben. 
#
# Der typische Aufruf mit den nicht anzugebenden Defaultwerten ist dann wie folgt:
#       show_images(Bild-Matrix, Anzahl_Bilder_die_nebeneinander_dargestellt_werden_sollen) 

def show_images(imgs_to_show, anzahl_kachelspalten, hide_axes=False, gesamtplotbreite_in_pixel=2400):

  # Anzahl und Groesse der Bilder bestimmen
  # Es gilt: shape (anzahl, size_y, size_x, farbkanaele)
  anzahl_bilder, size_y, size_x,_ = imgs_to_show.shape
  plot_cols = anzahl_kachelspalten
  N = gesamtplotbreite_in_pixel # Mit 2400 ist man schon gut dabei denn Monitore mit viel mehr Pixel je Zeile
                                # gibt es kaum. Falls weniger als 2400 Pixel zur Verfuegung stehen, dann
                                # wird die Gesamtausgabe durch matplot automatisch kleiner gerechnet,
                                # was aber Rechenzeit kostet

  
  if (plot_cols > 1):
     # Anzahl der Subplots je Zweile und Spalte bestimmen 
    plot_rows = anzahl_bilder // (plot_cols )  # Zur Sicherheit fuer den Rest auch noch eine zusaetzliche Zeile spendieren

    if ( ((anzahl_bilder % plot_cols ) !=0) ):   # Wenn "Reste" da sid dann benoetigen wir eine Zeile mehr fuer diese Reste
      plot_rows = plot_rows +1

    fig, axes = plt.subplots(nrows=plot_rows, ncols=plot_cols)
    #print("fig.dpi:",fig.dpi)


    # Die Groesse fuer die Gesamtausgabe berechnen, also die Groesse ueber alle Bilder                                                                                      
    # mit maximal 32x32 (64x64) Pixel 
    # Begrenzt ist die Breite der Ausgabe, die der Monitor nur begrenz breit ist. 
    # Die Hoehe ist nicht begrenzt, da der Monitor "scrollen kann". 
    # Wir gehen davon aus, dass in einer Zeile mindesten N Pixel ausgegeben werden koennen.
    # Die zur Verfuegung stehende Breite in inch ist somit N / dpi. Die guelte dpi erhalten wir ueber fig.dpi
    # In diesen N/dpi inch werden immer die plot_cols Bilder dargestellt, zur Not verkleinert wenn es nicht passt.
    # Die in der vertikaln Richtung beoetigte Groesse ergibt sich proportional ueber
    # a.) das Verhaeltnis von Hoehe und Breite der vielen kleinen darzustellenden Einzelbilder
    # b.) dem Verhaeltnis von plot_rows und plot_cols

    fig.set_size_inches( N/fig.dpi , N/fig.dpi * (plot_rows * size_y)/(plot_cols * size_x) ) # Breite horizontal, Breite vertikal

    if (axes.ndim == 1):                  # axes sollte immer "echt" zweidimensional sein. Ansonsten muesten noch mehr Fallunterscheidungen
      axes = axes.reshape(1,axes.size )   # eingebaut werden
    
    # Alle Achsenbeschriftungen deaktivieren, denn diese machen bei Bildern keinen wirklichen Sinn
    if (hide_axes == True):
      for y in range(plot_rows):                
        for x in range(plot_cols):
          axes[y,x].xaxis.set_visible(False)  # Das axes hat ZWEI DIMENSION
          axes[y,x].yaxis.set_visible(False)  # Das axes hat ZWEI DIMENSION

    # Ausgabe
    y = 0
    x = 0
    for i in range(anzahl_bilder) :
      axes[y,x].matshow(imgs_to_show[i])    # Das axes hat ZWEI DIMENSION
      if (x < plot_cols-1 ) :
        x = x + 1
      else:
        x = 0
        y = y + 1   

  else: 

    # Nur ein Bild pro Zeile ausgeben
    # Leider ist diese Fallunterscheidung notwending weil plt.subplots abhaengig von der Parametrierung
    # ein axes mit verschiedenen Dimensionen zurueck gibt:    1D-Subplots bei einem Subplot (einer Spalte) pro Zeile
    #                                                         2D-Subplots bei mehreren Spalte
    #                                                         0D-Subplots bei einen einzigen Bild ueberhaupt
    # Die verwendten Mechanismen sind die gleichen wie oben.
    # Die Komentare von oben gelten entsprechend.
    # Hier wird die Achsenbeschriftung nicht deaktiviert, denn in der Regel ist genuegend Platz
    plot_rows = anzahl_bilder   

    fig, axes = plt.subplots(nrows=plot_rows, ncols=1)

    fig.set_size_inches( N/fig.dpi , N/fig.dpi * (plot_rows * size_y)/(1 * size_x) ) # Breite horizontal, Breite vertikal

    for i in range(anzahl_bilder) :
      if (anzahl_bilder > 1):
        axes[i].matshow(imgs_to_show[i])    # Das axes hat EINE DIMENSION
      else:
        axes.matshow(imgs_to_show[i])       # Das axes hat NULL DIMENSION
      
  return 



import keras as keras
from keras import models
#from keras import layers  
#from keras import optimizers
#from keras import losses
#from keras import metrics
#from keras import activations
#from keras import Input

#from tensorflow.keras import initializers
#from keras import utils               # Hiervon wird to_categorical() benötigt        

# Fuer das Handling von Bildern
#from keras.preprocessing.image import load_img
#from keras.preprocessing.image import save_img
#from keras.preprocessing.image import img_to_array




##################################################################################################
#                Activation-Maps als Bilder in Kachelanordnung ausgeben                          #
################################################################################################## 
# Eine Funktion, die die Activation-Maps eines Layers(layernummer) in einem Netz (network)
# anhand eines Bildes (bildnummer) aus einer Menge von Bildern (matrix_mit_allen_bildern)
# berechnet und nebeneinander (jeweils anzahl_kachelspalten in einer Reihe)
# ausgibt. Die weiteren Parameter (hide_axes, gesamtplotbreite_in_pixel) sind identisch
# zu den Parametern der Funktion show_images().

def show_activation_maps_of_one_2D_cnn_layer(netzwerk, layer_nummer, matrix_mit_allen_bildern, bildnummer, anzahl_kachelspalten, hide_axes=False, gesamtplotbreite_in_pixel=2400):

  # Parameter
  TRAININGSBILDNUMMER = bildnummer     # Numer des Trainingsbildes anhand dessen die Activation-Maps berechnet werden sollen   
  LAYER = layer_nummer                 # Index des Layers der untersucht werden soll. Indizierung starten oben mit 0
                              # ACHTUNG:  models.Model() erlaub es (warum auch immmer) nicht, dass man als Paramter outout=
                              #           den Output eines Functional-Layers (hier LAYER=1) angibt.
                              #           Als Workaround kann man aber den Output des Functional-Layers auf
                              #           einen die Daten nicht veraendernden Layer geben (z.B. layers.Cropping2D(cropping=((0, 0), (0, 0))) (),
                              #           also einen Layer der oben und unten sowie rechts und links 0 Pixel abschneidet)
                              #           und diese Layer (hier LAYER = 2) dann als Output bei models.Model() angeben. 
                            

  #### 1.) Ein Netzwerk definieren, dass die Activation-Maps als Ausgabe berechnet ####
  # Definition des Layers desen Activation-Maps ausgegeben werden sollen
  layer_outputs = netzwerk.layers[LAYER].output

  # Definition eines neuen Netzes das diese oben definierten Activation-Maps ausgibt
  netwerk_das_activation_maps_ausgibt = models.Model(inputs=netzwerk.input, outputs=layer_outputs)

  # Das Bild des Trainingsdatensatzes an dieses Netzwerk anlegen und die Ausgaben
  # (also die Activation-Maps) berechnen lassen
  feld_von_bildern = np.expand_dims(matrix_mit_allen_bildern[TRAININGSBILDNUMMER], axis=0)  # Das Netwerk verlangt formal ein Tensor von Bildern und nicht ein einzelnes Bild
                                                                                # Damit shape stimmt muss also ein feld_von_bildern mit nur einem Bild erzeugt werden

  #### 2.) Das Netwerk die Activation-Maps berechnen lassen  ####
  # Die Ausgaben (also die Activation-Maps auf Basis des Eingabebildes berechnen 
  activation_maps = netwerk_das_activation_maps_ausgibt.predict(feld_von_bildern) 

  # Die predict-Funktion liefert in der Regel die Aktivation-Maps fuer mehrere Eingabebilder.
  # Dies ist dann ein 4-D-Tensor [Eingabebildnummer], [y-Bildkoordinate] [x-Bildkoordinate] [kernel]
  # Da wir hier nur einen Layer haben muessen wir die Dimension [Eingabebildnummer] "herausnehmen".
  # Dies geschieht hier.
  activation_des_layers = activation_maps [0,:,:,:] #[0,:,:, 2:3] # Vorerst nur das eine Bild mit dem Index 2 nehmen



  #### 3.) Umformen der  Activation-Maps zu Bildern  ####
  # Die Activation-Maps so "umformatieren", dass aus jedem Kanal jeweils ein Bild erzeugt wird.
  # Dieses so dargestellte Bild soll dann vergleichbar sein mit dar Darstellung die entsteht,
  # wenn an die Matrix direkt mit "plt.matshow(activation_des_layers[ :, :, kernel])" 
  # und mittels Sub-Plot ausgibt.
  # Da show_images() Sub-Plots benutzt ist diese fuer Sub-Plots verlangte Umformung also notwendig.

  # Bei einer Activation-Map sind ist ein Bild quasi in einem Kanal.
  # Da aus jedem Kanalein Bild erzeugt werden soll muessen also Dimensionen 0 und 2 getauscht werden.
  bilder = np.swapaxes(activation_des_layers,0,2) # Dimension 0 und 2 tauschen

  # Da bei einer Activation-Map gegenueber Bildern wohl Zeilen udn Spaltebn vertauscht sind,
  # muessen die Dimensionen 1 und 2 getauscht werden.
  bilder = np.swapaxes(bilder,1,2)                # Dimesion 1 und 2 (also Zeilen und Spalten) tauschen

  #plt.matshow(bilder[0], cmap=plt.cm.binary_r)  # Zum Test das 0. Bild mit plt direkt also ohne Sub-Plot ausgeben
                                                # So wie es aussieht normiert plt.matshow() auf den Wertebereich von 0 bis 1
                                                

  # Die Pixelwerte so normieren dass diese im Wertebereich von 0 bis 1 liegen,
  # denn bei einem Sub-Plot geht man wohl von einem solchen Wertebreich aus.
  # Wenn man nicht in einem Sub-Plot (sondern in einem normalen Plot) ausgibt, dann wird wohl im Rahmen
  # des Plotten bei Bedarf automatisch auf diesen Bereich normiert.
  # Wo man von welchen Wertebereichen ausgeht und wo wie automatisch normiert wird ist noch zu im Detail zu klaeren.
  for i in range(bilder.shape[0]):
    temp = bilder[i] + 0        # Tiefe Kopie nach temp
    temp = temp - np.min(temp)  # Der niedrigste im "Bild" vorkommende Wert ist hiernach  nun 0
    temp = (temp/np.max(temp))  # Der hoechste im "Bild" vorkommende Wert ist hiernach nun 1
    bilder[i] = temp
    
  # Bei der darstellung von Bildern geht man wohl von 3 Farbkanaelen aus.
  # Bisher haben wir dafuer ueberhaubt noch gar keine Dimension.
  # Darum wird zunaecht diese Dimension geschafffen und dann aus dem einen Farbkanal 2 weiter 
  # Farbkanaele durch kopieren der Inhalte des einen Farbkanals erzeugt
  bilder = np.expand_dims(bilder, axis=3)  # Eine Dimension hinzufuegen weil wenigsten 1 Farbkanal vorhanden sein muss
  bilder = np.repeat(bilder, 3, axis=3) # Den einen Kanal (z.B. R) auf alle weiteren 2 Kanaele (G,B) kopieren
                                        # weil Sub-Plot von 3 Farbkanaelen ausgeht und ansonsten einen Fehler wirft

  #### 4.) Ausgabe der Bider, die aus den Activation-Maps durch Konvertierung entstanden sind ####
  show_images(bilder, anzahl_kachelspalten, hide_axes, gesamtplotbreite_in_pixel)

  return





